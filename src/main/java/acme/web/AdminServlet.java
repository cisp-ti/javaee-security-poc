package acme.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

@WebServlet("/admin")
@ServletSecurity(@HttpConstraint(rolesAllowed = { "ADMIN" }))
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(AdminServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.debug("executing get method on Admin Servlet");

		Principal userPrincipal = request.getUserPrincipal();
		String message = String.format("User name [%s] in role ADMIN [%s] in role WEBSERVICE [%s]",
					userPrincipal.getName(),
					request.isUserInRole("ADMIN"),
					request.isUserInRole("WEBSERVICE"));
		request.setAttribute("message", message);

		RequestDispatcher dispatcher =
				request.getRequestDispatcher("/show-message.jsp");
		dispatcher.forward(request, response);
	}
}
