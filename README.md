## Java EE Security

### Wildfly

O exemplo está configurado para usar o domain do wildfly, ver arquivo jboss-web.xml:

	<security-domain>java:/jaas/cisp-ldap</security-domain>

As roles utilizadas são ADMIN e WEBSERVICE, e delimitam os resources da seguinte forma:

- As roles ADMIN, WEBSERVICE podem acessar /guest

- Apenas a role ADMIN pode acessar /admin

### Teste

*NOTA* Verificar se os usuários se encontram no domain (LDAP?) e possuem as respectivas senhas.

curl --basic -u ws99901:teste10 http://localhost:8080/simple-authentication-webapp/guest

curl --basic -u ws99901:teste10 http://localhost:8080/simple-authentication-webapp/admin

curl --basic -u web002:teste10 http://localhost:8080/simple-authentication-webapp/guest

curl --basic -u web002:teste10 http://localhost:8080/simple-authentication-webapp/admin
